# Assignment 3: Socket Programming
This project is created to demonstrate how socket programming works, by making TCP requests from one host to another host with Master-Worker architecture.

## Group: C2
* Alvin Hariman - 1806205432
* Jovi Handono Hutama - 1806205161
* Michael Susanto - 1806205653

## Techstack
* Python 3
* Amazon EC2

## Local Setup
* Clone this repository
```cmd
git clone https://gitlab.com/compnet-c2/tugas-3.git
```

* Go inside this project
```cmd
cd tugas-3
```

* If you want to run both master and worker in local, change the host and port to (then save):
```cmd
host = '127.0.0.1'
port = 1234
```

* Open two terminals

* Run worker on one terminal
```cmd
python worker.py
```

* Run master on the other terminal
```cmd
python master.py
```

* From master.py, try to send message with format:
```cmd
<message> <delay>
```

* Example: thisIsSampleMessage 5

* Observe the result on both terminals

## Features
### Master
* Master can send message and delay to worker, asynchronously.
* Get notified about worker's status job.
* Send TCP Connection Request to Worker.
* Determine job types
	* lama (>= 1 minute)
	* singkat (1 to 59 seconds)
	* sangat singkat (< 1 second)

### Worker
* Worker can receive data from master.
* Send information about themselves.
* Do their job: uppercase the message.
* Jobs are executed based on FCFS (First Come First Serve) Algorithm with Queue.

## Future Improvements
* Load Balancer
* Upscaling / Downscaling