# Import modules 
import socket, datetime, threading, os, time
from _thread import *

# Define the port on which you want to connect
host = os.getenv('ec2_worker_1_host')
port = os.getenv('ec2_worker_1_port')

# process user input
def input_sentence(message) :
    # Waiting Worker to receive connection and job
    print("Waiting worker to connect . . .")

    # open TCP connection
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM) 
    s.connect((host,int(port)))

    # get information about which worker will execute this job
    worker, status, i = s.recv(1024).decode().split()
    print("[Worker " + i + ": " + worker + "]: " + status)

    # calculate start time execution
    now = datetime.datetime.now()

    # send message
    s.send(message.encode())

    # receive status
    status = s.recv(1024).decode()
    print("[Worker " + i + ": " + worker + "]: " + status)

    # receive processed message from worker
    data, status = s.recv(1024).decode().split()
    
    # calculate the execution time (sending & receiving data)
    delta = datetime.datetime.now() - now
    
    # determine job based on its execution time
    job_type = ""
    if(delta >= datetime.timedelta(minutes=1)) :
        job_type = "lama"
    elif(delta >= datetime.timedelta(seconds=1)) :
        job_type = "singkat"
    else :
        job_type = "sangat singkat"

    # print received message
    print("[Worker " + i + ": " + worker + "]: " + status + ", Received: " + data + ", Type: " + job_type)
    
    # close connection
    s.close()

# Main program
def Main():

    # asks user input forever
    while 1:

        time.sleep(0.5)

        # get user input from terminal with format "<message> <delay>"
        print("Enter lowercase sentence and delay (format: <message> <delay>):")
        user_input = input()
        
        # start execution on thread
        try:
            start_new_thread( input_sentence, (user_input, ) )
        except:
            print ("Error: unable to start thread")

# Run program by directly executing the .py file
if __name__ == '__main__': 
    Main()