# Import modules 
from socket import *                 
from _thread import *
import threading, time, os

# define queue, thread, flag, and counter
queue = []
thread = []
flag = []
i = 0

# print status
def status(status, i) :
    while flag[i] :
        time.sleep(1)
        print(status + "running")        

    print(status + "stopped")

# Convert to uppercase
def upper(i) :

    # get connection from queue
    conn_socket = queue.pop(0)

    # send information about this worker
    info = str(os.getpid())
    info += " connected"
    info += " " + str(i)
    running = "running"
    finished = " finished"
    conn_socket.send(info.encode())

    # receive data from master
    raw, delay = conn_socket.recv(1024).decode().split(" ")
    conn_socket.send(running.encode())
    time.sleep(int(delay))

    # process data and send it back
    capitalized_sentence = raw.upper()
    capitalized_sentence += finished
    conn_socket.send(capitalized_sentence.encode())
    flag[i] = False

# Define the port on which you want to connect 
worker_host = os.getenv('ec2_worker_1_host')
worker_port = os.getenv('ec2_worker_1_port')

# Create a socket object 
worker_socket = socket(AF_INET, SOCK_STREAM)
worker_socket.bind((worker_host, int(worker_port)))
worker_socket.listen(5)
print("Worker is listening at port", worker_port)

while True:
    # Accept TCP connection
    conn_socket, addr = worker_socket.accept()
    
    # check connection's validity
    if(conn_socket is not None) :
        print("Connected to address: ", addr)

        # add connection to queue
        i = i + 1
        queue.append(conn_socket)
        flag.append(True)

        # create thread to print execution's status
        start_new_thread( status, ("Status thread " + str(i) + ": ", i-1) )        
    
    # do the job
    upper(i-1)

# Close TCP connection
conn_socket.close()